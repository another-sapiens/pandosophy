import { reactive } from "vue";
// This file is part of Pandosophy
// Pandosophy is a web app built with Vue. Among its goals are serving as an educational tool
// by providing an example of the framework's usage,
// and serving as a template for developers intending to create their own project.

// Copyright (C) 2022, Andrés Schafer <altersapiens@tutanota.com>

//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.

//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { ref } from "vue";
import { useLocalStorage } from "@vueuse/core";

interface Theme {
	actualName: string;
	stringName: string;
	background: string;
	foreground: string;
	font: string;
	highlightedFont: string;
	accent: string;
	bigContainer: string;
	textInput: string;
	textInputFont: string;
	accentFont: string;
	disabled: string;
	disabledFont: string;
}

export interface Themes {
	[key: string]: Theme;
}

export const themes = reactive(<Themes>{
	//Define your themes here!  :)

	bambooLight: <Theme>{
		actualName: "Bamboo Light",
		stringName: "bamboo-light",
		background: "#1b5e20",
		foreground: "#2e7d32",
		font: "#1C1C1C",
		highlightedFont: "beige",
		accent: "#4caf50",
		bigContainer: "beige",
		textInput: "beige",
		textInputFont: "#1C1C1C",
		accentFont: "#1C1C1C",
		disabled: "#17521c",
		disabledFont: "#a1a190",
	},

	bambooDark: <Theme>{
		actualName: "Bamboo Dark",
		stringName: "bamboo-dark",
		background: "#0d1e00",
		foreground: "#172C08",
		font: "#f2f2c2",
		highlightedFont: "#f2f2c2",
		accent: "#34421E",
		bigContainer: "#0C1110",
		textInput: "#f2f2c2",
		textInputFont: "#0d1e00",
		accentFont: "#f2f2c2",
		disabled: "#132406",
		disabledFont: "#66665c",
	},
	yin: <Theme>{
		actualName: "Yin",
		stringName: "yin",
		background: "black",
		foreground: "#131313",
		font: "#F2F2F2",
		highlightedFont: "#F2F2F2",
		accent: "#d1d1d1",
		bigContainer: "#121212",
		textInput: "#111111",
		textInputFont: "#F2F2F2",
		accentFont: "#1C1C1C",
		disabled: "#090909",
		disabledFont: "#3b3b35",
	},
	yang: <Theme>{
		actualName: "Yang",
		stringName: "yang",
		background: "#e3e3e3",
		foreground: "#F2F2F2",
		font: "#1C1C1C",
		highlightedFont: "#292929",
		accent: "#292929",
		bigContainer: "white",
		textInput: "#F2F2F2",
		textInputFont: "#1C1C1C",
		accentFont: "#F0F0F0",
		disabled: "#cccccc",
		disabledFont: "#66665c",
	},
});

// Saves the theme to local storage
export const themeName = ref(useLocalStorage("theme", "bamboo-light"));

// The values themeStr holds are the same as any theme's stringName,
// and will be matched against them to know which one we're using
const themeStr = ref(themeName);

// The actual theme, converted to CSS
export const theme = ref(themeToCss());

export function getTheme(themeStr: string) {
	// this loops through every theme in the themes object,
	// and returns the theme for which its stringName matches the themeStr
	for (const [declaredTheme, themeProps] of Object.entries(themes)) {
		if (themeStr === themeProps.stringName) return themes[declaredTheme];
	}
}

export function getThemeList(): string[] {
	// loops through the themes object, and returns the stringName of every theme
	let themeList: string[] = [];
	for (const [theme, themeValues] of Object.entries(themes)) {
		themeList.push(themeValues.stringName);
	}
	return themeList;
}

export function changeTheme(newTheme: string) {
	themeName.value = newTheme;
	theme.value = themeToCss();
}

export function themeToCss() {
	// Prepends '--' and appends ';' to each value pair
	// of the theme object that matches the themeStr.
	// This turns every property into a valid CSS variable
	return Object.entries(getTheme(themeStr.value)!)
		.map(([key, value]) => `--${key}:${value}`)
		.join(";");
}
