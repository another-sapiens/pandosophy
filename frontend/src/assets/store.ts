import * as backendVars from "@/backendVars.json";

import { reactive, ref } from "vue";

export const backendUrl = backendVars.url;

export const user = reactive({
	isAuth: false,
	name: "",
	image: "",
});

export function declareAuth() {
	user.isAuth = true;
}

export function setUsername(name: string) {
	user.name = name;
}

// Set document scrollability
export function setScrollable(isScrollable: boolean) {
	const scrolling = isScrollable ? "auto" : "hidden";
	document.documentElement.style.overflow = scrolling;
}

export async function getProfileImg(username: string) {
	try {
		const response = await fetch(`${backendUrl}/users/${username}/image`);
		const data = await response.json();
		user.image = data.image;
	} catch {
		console.log(`Couldn't fetch profile image. Using generic icon.`);
		user.image = `${backendUrl}/images/generic.svg`;
	}
}
