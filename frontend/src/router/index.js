// This file is part of Pandosophy
// Pandosophy is a web app built with Vue. Among its goals are serving as an educational tool
// by providing an example of the framework's usage,
// and serving as a template for developers intending to create their own project.

// Copyright (C) 2022, Andrés Schafer <altersapiens@tutanota.com>

//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU Affero General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.

//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU Affero General Public License for more details.

//     You should have received a copy of the GNU Affero General Public License
//     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import { createRouter, createWebHistory } from "vue-router";

const routes = [
	{
		path: "/",
		name: "All Posts",
		component: () => import("/src/views/PostsPage.vue"),
	},
	{
		path: "/lore",
		name: "Lore",
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () =>
			import(/* webpackChunkName: "about" */ "/src/views/Lore.vue"),
	},
	{
		path: "/shop",
		name: "Shop",
		component: () => import("/src/views/Shop.vue"),
	},
	{
		path: "/settings",
		name: "Settings",
		component: () => import("/src/views/Settings.vue"),
	},
	{
		path: "/about",
		name: "About",
		component: () => import("/src/views/About.vue"),
	},
	{
		path: "/login",
		name: "Login",
		component: () => import("/src/views/Login.vue"),
	},
	{
		path: "/sign-up",
		name: "Sign Up",
		component: () => import("/src/views/SignUp.vue"),
	},
	{
		path: "/users/:username",
		name: "User Profile",
		component: () => import("/src/views/UserInfo.vue"),
	},
];

const router = createRouter({
	history: createWebHistory(import.meta.env.BASE_URL),
	routes,
});

export default router;
