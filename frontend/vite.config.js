import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { fileURLToPath } from "url";
import path from "path";

export default defineConfig({
	plugins: [vue()],
	base: "",
	server: { port: 8080 },
	resolve: {
		alias: {
			"@": path.resolve(
				path.dirname(fileURLToPath(import.meta.url)),
				"src"
			),
		},
	},
	css: {
		preprocessorOptions: {
			scss: {
				additionalData: `

				@import "./src/assets/_variables.scss";

				`,
			},
		},
	},
});
