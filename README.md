# Pandosophy

Arise, panda lovers! Take a sneak peek at Panda's inner thoughts with this beautiful web app.

The site is written in Typescript, and uses Vue 3 for the frontend. Its main purpose is to showcase Vue's capabilities, to innovate, and to share these innovations with the world!

<img src="./frontend/screenshots/posts-responsive.png" alt="drawing" width="10000"/>
<img src="./frontend/screenshots/bamboo-light.jpg" alt="drawing" height="300"/>

<img src="./frontend/screenshots/bamboo-dark.jpg" alt="drawing" height="300"/>
<img src="./frontend/screenshots/lore.jpg" alt="drawing" height="300"/>

<img src="./frontend/screenshots/yin.jpg" alt="drawing" height="300"/>
<img src="./frontend/screenshots/yang.jpg" alt="drawing" height="300"/>
<img src="./frontend/screenshots/login.jpg" alt="drawing" height="300"/>

# How do I get started?

Well, that depends on you, dear user!
Pandosophy is composed of a frontend and a backend. I'll assume you want the all-in-one solution, but don't let that stop you from using the server with your own UI (or viceversa) if that's what you need.

Keep in mind that the frontend will fetch most of its content from the server, so you may want to set that up first.

You can follow all the instructions to get the project up and running, or use only one section.

---

First, you'll have to download this project.

(Note that you'll need a recent version of Node for this to work)

## Server

The server uses Mongoose to connect to a MongoDB database. Naturally, this means you need access to such database.

You can do so by creating an account at MongoDB Atlas, or self-managing it. I opt for the latter, but [this](https://www.mongodb.com/basics/create-database) might help you if you choose the first one.

Once you have the address for your database, open the project and go to:

-   server/.env.example

Change the name from `.env.example` to `.env` and edit the variables to match your addresses.

Now open your terminal on the project's root folder and type:

```
cd server && npm install
```

This installs all the dependencies.

Then type:

```
npm run start
```

This runs the server.

For development, it is recommended to use another command instead:

```
npm run dev
```

This enables hot-reload, so you don't have to restart the server after changing the code. Keep in mind this will make requests fail while it's reloading.

## Frontend

A-ha! The interesting part.

First off, we need to specify what server we want to use. If you're the one hosting the backend locally, it should be your local IP address.

Open the project folder. Go to:

-   frontend/src/backendVars.json

and change the "url" to your local IP address. Or to any URL where it's being served. This is not strictly necessary, but without this step no posts will be visible.

Now open the project's root folder in your terminal and type:

```
cd frontend && npm install
```

This installs all the dependencies.

Now, let's run it:

```
npm run dev
```

The terminal should tell you where you can access the site. If you can't see this, you can still access this address from your browser:

```
http://localhost:8080/
```

When you had enough fun, you can build the project:

```
npm run build
```

The output files will be on

-   frontend/dist/

As per Vue's docs, you can't run the html file as-is. You must serve them first.
Once built and served, the app should run much faster than in dev mode!

---

That's all you need!

Enjoy :)
