import dotenv from "dotenv";
dotenv.config();
import { updateImgUrl } from "./definedMethods";

import express from "express";
const app = express();

import mongoose from "mongoose";
import cors from "cors";

import session from "express-session";
declare module "express-session" {
	interface SessionData {
		isAuth: boolean;
		username: string;
	}
}
const MongoStore = require("connect-mongodb-session")(session);

const PORT = parseInt(process.env.DB_URL!) || 8081;

// Put the URL for your frontend here
const whiteList = [
	"http://192.168.0.13:8080",
	"http://localhost:8080",
	"http://localhost:3334",
];

app.use(express.json());
app.use(express.static("public"));
app.use(
	cors({
		origin: whiteList,
		credentials: true,
	})
);

const store = new MongoStore({
	uri: process.env.DB_URL,
	collection: "sessions",
});
app.use(
	session({
		secret: process.env.SECRET_KEY as string,
		cookie: { maxAge: 60000 },
		resave: false,
		saveUninitialized: false,
		store,
	})
);

//Middleware
import posts from "./routes/posts";
import users from "./routes/users";
app.use(posts);
app.use(users);

//Connect to DB
mongoose.connect(process.env.DB_URL!, () =>
	console.log("%cConnected to database", "color: green")
);

//Listen
app.listen(PORT, () => updateImgUrl());
