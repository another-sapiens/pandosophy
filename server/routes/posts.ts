import express from "express";

import Post from "../models/Post";
import User from "../models/User";

const router = express.Router();

const rootPath = "/posts";

//Check if logged in
const isAuth = (req: any, res: any, next: any) => {
	if (req.session.isAuth) {
		next();
	} else {
		res.json("log in first");
	}
};

router.get(rootPath, async (req, res) => {
	try {
		const posts = await Post.find().sort({ paws: "desc" });
		res.json(posts);
	} catch (error) {
		res.json({ message: error });
	}
});

// Post a post
router.post(rootPath, isAuth, async (req, res) => {
	try {
		const user = await User.findOne({ username: req.session.username });

		const newPost = new Post({
			name: req.session.username,
			quote: req.body.quote,
			year: 2022,
			image: user.image,
			localImage: user.image.includes("http") ? false : true,
		});

		await newPost.save();
		res.json(await Post.find());
	} catch (error) {
		res.json({ message: error });
	}
});

//Get one post
router.get(`${rootPath}/:postId`, async (req, res) => {
	try {
		const post = await Post.findById(req.params.postId);
		res.json(post);
	} catch (error) {
		res.json({ message: error });
	}
});

//Delete post
router.delete(`${rootPath}/:postId`, isAuth, async (req, res) => {
	console.log("deleting post");

	try {
		const deletedPost = await Post.findById(req.params.postId);
		if (req.session.username === deletedPost.name) {
			await Post.deleteOne({ _id: req.params.postId });
			res.json(deletedPost);
		}
	} catch (error) {
		res.json({ message: error });
	}
});

//Paw a post
router.patch(`${rootPath}/:postId/paw`, isAuth, async (req, res) => {
	try {
		const user = req.session.username;
		const post = await Post.findById(req.params.postId);

		let isPawed = false;
		for (const pawer of post.pawedBy) {
			isPawed = pawer === user;
		}

		if (!isPawed) {
			// Adds one paw and pushes the username to a list
			await Post.updateOne(
				{ _id: req.params.postId },
				{ $inc: { paws: 1 }, $push: { pawedBy: user } }
			);
			// Gets updated amount of paws
			const pawedPost = await Post.findById(req.params.postId);
			// Sends back new amount of paws
			res.json({ newPaws: pawedPost.paws, pawedBy: pawedPost.pawedBy });
		} else {
			// Does the opossite of the if statement above
			await Post.updateOne(
				{ _id: req.params.postId },
				{ $inc: { paws: -1 }, $pull: { pawedBy: user } }
			);
			const pawedPost = await Post.findById(req.params.postId);
			res.json({ newPaws: pawedPost.paws, pawedBy: pawedPost.pawedBy });
		}
	} catch (error) {
		console.log("could not paw post");

		res.json({ message: error });
	}
});

export default router;
