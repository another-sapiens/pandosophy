import express from "express";
import User from "../models/User";
import bcrypt from "bcrypt";
import multer from "multer";
import mime from "mime";

const router = express.Router();

const rootPath = "/users";

const imageStorage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, "public/images");
	},
	filename(req, file, cb) {
		const fileExt = mime.extension(file.mimetype);
		const uniqueSuffix = `${Date.now()}-${Math.round(
			Math.random() * 1e9
		)}.${fileExt}`;
		cb(null, file.fieldname + "-" + uniqueSuffix);
	},
});

const imageUpload = multer({ storage: imageStorage }).single("avatar");

// Find a user by ID
router.get(`${rootPath}/:id`, async (req, res) => {
	try {
		res.json(await User.findById(req.params.id).select("-_id -password"));
	} catch (error) {
		res.json(error);
	}
});

// Get a user's profile image
router.get(`${rootPath}/:username/image`, async (req, res) => {
	try {
		const image = await User.findOne({ username: req.params.username })
			.select("image -_id")
			.lean();
		res.send(image);
	} catch (error) {
		res.send(error);
	}
});

// Get a user's bio
router.get(`${rootPath}/:username/bio`, async (req, res) => {
	try {
		const bio = await User.findOne({ username: req.params.username })
			.select("bio -_id")
			.lean();
		res.json(bio);
	} catch (error) {
		res.send(error);
	}
});

// Upload an image
router.post(`${rootPath}/upload-image`, (req, res) => {
	imageUpload(req, res, function (err) {
		if (err) {
			console.log(err);
			res.json("could not save image");
		} else {
			console.log(`uploaded image: ${res.req.file?.filename}`);
			res.json(res.req.file?.filename);
		}
	});
});

// Sign up
router.post(`${rootPath}/sign-up`, async (req, res) => {
	try {
		const hashedPassword = await bcrypt.hash(req.body.password, 10);

		const newUser = new User({
			username: req.body.username,
			password: hashedPassword,
			image:
				req.body.image ||
				`${process.env.BACKEND_URL}:${process.env.PORT}/images/generic.svg`,
			bio: req.body.bio || "No wisdom here.",
			species: req.body.species,
		});

		await newUser.save();
		req.session.isAuth = true;
		req.session.username = req.body.username;
		res.json(req.session);
	} catch (error) {
		res.json(error);
	}
});

// Login
router.post(`${rootPath}/login`, async (req, res) => {
	const user = await User.findOne({ username: req.body.username });
	if (!user) return res.status(403);

	try {
		if (await bcrypt.compare(req.body.password, user.password)) {
			req.session.isAuth = true;
			req.session.username = user.username;
			res.json(req.session);
		}
	} catch (error) {
		console.log(error);

		res.json("Incorrect username or password.");
	}
});

export default router;
