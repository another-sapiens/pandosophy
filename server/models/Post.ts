import mongoose from "mongoose";

const PostSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	fallbackImgPath: {
		type: String,
		default: `${process.env.BACKEND_URL}:${process.env.PORT}/images/`,
	},
	image: {
		type: String,
		default: "pandonymous.jpg",
	},
	quote: {
		type: String,
		required: true,
	},
	year: {
		type: Number,
		required: true,
	},
	paws: {
		type: Number,
		default: 0,
	},
	localImage: {
		type: Boolean,
		default: false,
	},
	pawedBy: [String],
});
export default mongoose.model("Posts", PostSchema);
