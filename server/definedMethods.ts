import { log } from "console";
import Post from "./models/Post";

export async function updateImgUrl() {
	// This updates the path for the local images, so it points at the current IP address
	try {
		log("Updating the URL of locally served images...");

		const localImages = await Post.updateMany(
			{ localImage: true },
			{
				$set: {
					fallbackImgPath: `${process.env.BACKEND_URL}:${process.env.PORT}/images/`,
				},
			}
		);
		log("Local image URLs modified: ");
		log(localImages.modifiedCount ? localImages : 0);
	} catch (error) {
		log(error);
	}
}
